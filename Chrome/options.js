'use strict';

/**
 * Contains logic for the options page.
 */

/** Load options from chrome sync storage */
function LoadOptions(){
  chrome.storage.sync.get('stash', function (data) {
    const stash = data && data.stash ? data.stash : '';
    document.getElementById('stash').value = stash;
  });

  chrome.storage.sync.get('regexIssueNumber', function (data) {
    const regexIssueNumber = data && data.regexIssueNumber ? data.regexIssueNumber : null;
    
    if (regexIssueNumber != null)
      document.getElementById('regexIssueNumber').value = regexIssueNumber;
  });

  chrome.storage.sync.get('regexTagStop', function (data) {
    const regexTagStop = data && data.regexTagStop ? data.regexTagStop : null;

    if (regexTagStop != null)
      document.getElementById('regexTagStop').value = regexTagStop;
  });

  chrome.storage.sync.get('regexTagWip', function (data) {
    const regexTagWip = data && data.regexTagWip ? data.regexTagWip : null;
    
    if (regexTagWip != null)
      document.getElementById('regexTagWip').value = regexTagWip;
  });

  chrome.storage.sync.get('regexTagHotfix', function (data) {
    const regexTagHotfix = data && data.regexTagHotfix ? data.regexTagHotfix : null;

    if (regexTagHotfix != null)
      document.getElementById('regexTagHotfix').value = regexTagHotfix;
  });

  chrome.storage.sync.get('regexTagEpic', function (data) {
    const regexTagEpic = data && data.regexTagEpic ? data.regexTagEpic : null;

    if (regexTagEpic != null)
      document.getElementById('regexTagEpic').value = regexTagEpic;
  });
}

/** Save options to chrome sync storage */
function SaveOptions(){
  const submitBtn = document.getElementById('submit');
  submitBtn.setAttribute('disabled', 'disabled');

  const stash = document.getElementById('stash').value;
  chrome.storage.sync.set({ stash: stash });

  const regexIssueNumber = document.getElementById('regexIssueNumber').value;
  chrome.storage.sync.set({ regexIssueNumber: regexIssueNumber });

  const regexTagStop = document.getElementById('regexTagStop').value;
  chrome.storage.sync.set({ regexTagStop: regexTagStop });

  const regexTagWip = document.getElementById('regexTagWip').value;
  chrome.storage.sync.set({ regexTagWip: regexTagWip });

  const regexTagHotfix = document.getElementById('regexTagHotfix').value;
  chrome.storage.sync.set({ regexTagHotfix: regexTagHotfix });

  const regexTagEpic = document.getElementById('regexTagEpic').value;
  chrome.storage.sync.set({ regexTagEpic: regexTagEpic });

  window.setTimeout(() => { chrome.runtime.reload(); }, 1000);
}

document.addEventListener('DOMContentLoaded', function () {
  const submitBtn = document.getElementById('submit');
	submitBtn.addEventListener('click', function () {
		SaveOptions();
	});
});

/** Load options when page is loaded */
LoadOptions();