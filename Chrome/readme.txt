v0.1.9
- CHANGE: New icons
- CHANGE: New Colors
- CHANGE: Code cleanup and small bug fixes
- ADDED: EPIC tag
- ADDED: Better issue number in DIFF view with Jira link

v0.1.8
- CHANGE: Icon is always active

v0.1.7
- FIX: Extension not lighting up icon on fresh install
- FIX: Matching PR title input field
- CHANGE: Options page regex's

v0.1.6
- Just a re-deploy

v0.1.5
- Added icons option

v0.1.4
- ADD: MaterializeCSS styling

v0.1.3
- FIX: running scripts on other sites than stash

v0.1.2
- FIX: Minor bugs
- CHANGE: Default startup values
- DEPT: Code Cleanup and refactoring
- DEPT: Code Comments

v0.1.1
- FIX: Fixed bug where icon and options would not light up on correct page

v0.1.0
- ADD: dropdown menu to toggle things
- ADD: highlight tags: ["DO NOT MERGE", "DO-NOT-MERGE", "WIP", "HOTxxx"].
- ADD: Make "IP-xxxx:" bold.
- ADD: Disable merge button if "DO NOT MERGE" or "WIP" tag is present.