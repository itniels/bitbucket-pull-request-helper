/**
 * Contains logic that will be injected into page
 */

const observerConfig = { childList: true, subtree: true };
let isActive = true;
let showTags = true;
let useIcons = true;
let showIssueNumbers = true;
let diffNumber = true;
let disableMerge = true;
let pageContainsStopTag = false;
let debounce = null;
let stash;

// Regexes
let regexIssueNumber = 'IN-\\d+([\\w-]+)?(\\:)?';
let regexTagStop = '(\\s?\\[DO(.)?NOT(.)?MERGE\\]\\s?)';
let regexTagWip = '(\\s?\\[WIP\]\\s?)';
let regexTagHotfix = '(\\s?\\[(HOT)(.*)\\]\\s?)';
let regexTagEpic = '(\\s?\\[(EPIC)(.*)\\]\\s?)';

// String tags
const tagDoNotMerge = 'do-not-merge-tag';
const tagWip = 'wip-tag';
const tagHotfix = 'hotfix-tag';
const tagEpic = 'epic-tag'

// load settings on load
loadSettings();

/** Load settings from storage */
function loadSettings() {
	chrome.storage.sync.get([
		'stash',
		'isActive',
		'showTags',
		'useIcons',
		'showIssueNumbers',
		'diffNumber',
		'disableMerge',
		'regexIssueNumber',
		'regexTagStop',
		'regexTagWip',
		'regexTagHotfix',
		'regexTagEpic'
	], function (data) {
		stash = data.stash;
		isActive = data.isActive;
		showTags = data.showTags;
		useIcons = data.useIcons;
		showIssueNumbers = data.showIssueNumbers;
		diffNumber = data.diffNumber;
		disableMerge = data.disableMerge;
		
		if (data.regexIssueNumber)
			regexIssueNumber = data.regexIssueNumber;

		if (data.regexTagStop)
			regexTagStop = data.regexTagStop;

		if (data.regexTagWip)
			regexTagWip = data.regexTagWip;

		if (data.regexTagHotfix)
			regexTagHotfix = data.regexTagHotfix;
		
		if (data.regexTagEpic)
			regexTagEpic = data.regexTagEpic;

		updatePage();
	});
}

/** Add listener for loading settings when they change */
chrome.storage.onChanged.addListener(function () {
	loadSettings();
});

/** Listen for DOM changes but limit to 100ms debounce to not go nuts! */
const observerCallback = function () {
	if (document.domain != stash || !isActive || debounce != null)
		return;

	debounce = window.setTimeout(() => {
		updatePage();
		debounce = null;
	}, 100)
};

const observer = new MutationObserver(observerCallback);
observer.observe(document.body, observerConfig);

/** Run through all checks and set the enabled tags */
function updatePage() {
	// Check if we are in the right place
	if (document.domain != stash)
		return;

	// Enclose everything in tags
	if (isActive)
		tagEverything(useIcons);

	// Highlight Tags
	highlightTags(isActive && showTags);

	// Icons
	showIcons(isActive && useIcons);

	// Hightlight Issue Numbers
	highlightIssueNumbers(isActive && showIssueNumbers);

	// Hide Merge Button
	disableMergeButton(isActive && disableMerge);

	// Replace 'Show Diff Of' with 'Diff of: <IssueNumber>'
	showIssueNumberAlways(isActive && diffNumber);
};

/** Highlights [] tags */
function highlightTags(highlight) {
	if (highlight) {
		$('.prh-dnm').addClass(tagDoNotMerge);
		$('.prh-wip').addClass(tagWip);
		$('.prh-hotfix').addClass(tagHotfix);
		$('.prh-epic').addClass(tagEpic);
	}
	else {
		$('.prh-dnm').removeClass(tagDoNotMerge);
		$('.prh-wip').removeClass(tagWip);
		$('.prh-hotfix').removeClass(tagHotfix);
		$('.prh-epic').removeClass(tagEpic);
	}
}

function showIcons(show) {
	if (show) {
		$('.prh-icon').removeClass('hidden');
		$('.prh-tag').addClass('hidden');
	}
	else {
		$('.prh-icon').addClass('hidden');
		$('.prh-tag').removeClass('hidden');
	}
}

/** Highlights the issue numbers */
function highlightIssueNumbers(highlight) {
	if (highlight)
		$('.prh-issuenumber').addClass('ip-tag');
	else
		$('.prh-issuenumber').removeClass('ip-tag');
}

function showIssueNumberAlways(isActive){
	if (!isActive)
		return;

	const issueNumber = $('.branch-name').get(0);
	const diffOfText = $('.aui-toolbar2-primary, .primary').find('h4').get(0);

	if (!issueNumber || !diffOfText)
		return

	if (diffOfText.innerHTML.indexOf('prh-diff-text') > 0)
		return;

	const link = `https://ipaper.atlassian.net/browse/${issueNumber.innerText}`;
	diffOfText.innerHTML = `Diff of <a class="prh-diff-text" href="${link}" target="blank" title="Open in Jira">` + issueNumber.innerText + '</a>';
}

/** Toggles the MERGE button */
function disableMergeButton(disable) {
	if (pageContainsStopTag && disable)
		$('#fulfill-pullrequest,.merge-button').addClass('hidden');
	else
		$('#fulfill-pullrequest,.merge-button').removeClass('hidden');
}

/** Adds span tags to all the items we want to control on the page */
function tagEverything(useIcons) {
	const items = $('.pull-request-title').not('.field-group');
	const items2 = $('.title').find('a');
	const header = $('#pull-request-header,.app-header').find('h1,h2').get(0);

	items.each((i, e) => {
		var result = addTags(e.innerText, useIcons);

		if (HtmlEncode(e.innerHTML).length < result.length)
			e.innerHTML = result;
	});

	items2.each((i, e) => {
		var result = addTags(e.innerText, useIcons);

		if (HtmlEncode(e.innerHTML).length < result.length)
			e.innerHTML = result;
	});

	if (header && header.innerHTML.indexOf('prh') <= 0)
		header.innerHTML = addHeaderTags(header.innerHTML, header.innerText, useIcons);
}

/** Adds SPAN tags to a single elements text string (eg. [WIP] IN-1234: My pull request) */
function addTags(text, useIcons) {
	let dnm = new RegExp(regexTagStop + '(?!</span>)', 'gi');
	let wip = new RegExp(regexTagWip + '(?!</span>)', 'gi');
	let hotfix = new RegExp(regexTagHotfix + '(?!</span>)', 'gi');
	let epic = new RegExp(regexTagEpic + '(?!</span>)', 'gi');
	let issueNumber = new RegExp(regexIssueNumber + '(?!</span>)', 'gi');

	// Calculate some state
	const hasDoNotMerge = dnm.test(text);
	const hasWip = wip.test(text);
	const hasPriority = hotfix.test(text);
	const hasEpic = epic.test(text);

	// Add icons
	// We should always show 3 icons!
	let result = '';

	if (hasEpic)
		result += GetPrhIcon('epic', useIcons, hasEpic, 'sm');
	else
		result += GetPrhIcon('hotfix', useIcons, hasPriority, 'sm');

	result += GetPrhIcon('wip', useIcons, hasWip, 'sm');
	result += GetPrhIcon('donotmerge', useIcons, hasDoNotMerge, 'sm');

	result += HtmlEncode(text);

	// IF useIcons we hide the text
	if (useIcons) {
		result = result.replace(dnm, `<span class="prh prh-tag prh-dnm hidden">$&</span>`);
		result = result.replace(wip, `<span class="prh prh-tag prh-wip hidden">$&</span>`);
		result = result.replace(hotfix, `<span class="prh prh-tag prh-hotfix hidden">$&</span>`);
		result = result.replace(epic, `<span class="prh prh-tag prh-epic hidden">$&</span>`);
	} else {
		result = result.replace(dnm, `<span class="prh prh-tag prh-dnm">$&</span>`);
		result = result.replace(wip, `<span class="prh prh-tag prh-wip">$&</span>`);
		result = result.replace(hotfix, `<span class="prh prh-tag prh-hotfix">$&</span>`);
		result = result.replace(epic, `<span class="prh prh-tag prh-epic">$&</span>`);
	}

	// If we are not a header we also add span to issue number
	result = result.replace(issueNumber, `<span class="prh prh-issuenumber">$&</span>`);

	return result;
}

/** Adds tags to a pull request header */
function addHeaderTags(html, text, useIcons) {
	let dnm = new RegExp(regexTagStop + '(?!</span>)', 'gi');
	let wip = new RegExp(regexTagWip + '(?!</span>)', 'gi');
	let hotfix = new RegExp(regexTagHotfix + '(?!</span>)', 'gi');
	let epic = new RegExp(regexTagEpic + '(?!</span>)', 'gi');

	const hasDoNotMerge = dnm.test(text);
	const hasWip = wip.test(text);
	const hasPriority = hotfix.test(text);
	const hasEpic = epic.test(text);

	// Hide merge button
	if (!pageContainsStopTag)
		pageContainsStopTag = hasWip | hasDoNotMerge;

	// Add icons
	// We should always show 3 icons!
	let result = '';

	if (hasEpic)
		result += GetPrhIcon('epic', useIcons, hasEpic, 'lg');
	else
		result += GetPrhIcon('hotfix', useIcons, hasPriority, 'lg');

	result += GetPrhIcon('wip', useIcons, hasWip, 'lg');
	result += GetPrhIcon('donotmerge', useIcons, hasDoNotMerge, 'lg');

	// Add text
	result += html;

	// IF useIcons we hide the text
	if (useIcons) {
		result = result.replace(dnm, `<span class="prh prh-tag prh-dnm hidden">$&</span>`);
		result = result.replace(wip, `<span class="prh prh-tag prh-wip hidden">$&</span>`);
		result = result.replace(hotfix, `<span class="prh prh-tag prh-hotfix hidden">$&</span>`);
		result = result.replace(epic, `<span class="prh prh-tag prh-epic hidden">$&</span>`);
	} else {
		result = result.replace(dnm, `<span class="prh prh-tag prh-dnm">$&</span>`);
		result = result.replace(wip, `<span class="prh prh-tag prh-wip">$&</span>`);
		result = result.replace(hotfix, `<span class="prh prh-tag prh-hotfix">$&</span>`);
		result = result.replace(epic, `<span class="prh prh-tag prh-epic">$&</span>`);
	}

	return result;
}

/** HTML encodes text */
function HtmlEncode(str) {
	return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/** Returns an SVG icon by name */
function GetPrhIcon(name, useIcons, active, size) {
	let classes = 'prh prh-icon';
	let sizeClass = 'prh-icon-' + size;

	if (!useIcons)
		classes += ' hidden';

	if (name === 'hotfix')
		return `<span title="HOTFIX"><svg xmlns="http://www.w3.org/2000/svg" class="${classes} ${active ? 'prh-icon-hotfix-active' : ''} ${sizeClass} prh-icon-hotfix" viewBox="0 0 384 512"><path d="M216 23.858c0-23.802-30.653-32.765-44.149-13.038C48 191.851 224 200 224 288c0 35.629-29.114 64.458-64.85 63.994C123.98 351.538 96 322.22 96 287.046v-85.51c0-21.703-26.471-32.225-41.432-16.504C27.801 213.158 0 261.332 0 320c0 105.869 86.131 192 192 192s192-86.131 192-192c0-170.29-168-193.003-168-296.142z"/></svg></span>`;

	if (name === 'wip')
		return `<span title="WORK IN PROGRESS"><svg xmlns="http://www.w3.org/2000/svg" class="${classes} ${active ? 'prh-icon-wip-active' : ''} ${sizeClass} prh-icon-wip" viewBox="0 0 512 512"><path d="M507.73 109.1c-2.24-9.03-13.54-12.09-20.12-5.51l-74.36 74.36-67.88-11.31-11.31-67.88 74.36-74.36c6.62-6.62 3.43-17.9-5.66-20.16-47.38-11.74-99.55.91-136.58 37.93-39.64 39.64-50.55 97.1-34.05 147.2L18.74 402.76c-24.99 24.99-24.99 65.51 0 90.5 24.99 24.99 65.51 24.99 90.5 0l213.21-213.21c50.12 16.71 107.47 5.68 147.37-34.22 37.07-37.07 49.7-89.32 37.91-136.73zM64 472c-13.25 0-24-10.75-24-24 0-13.26 10.75-24 24-24s24 10.74 24 24c0 13.25-10.75 24-24 24z"></path></svg></span>`;

	if (name === 'donotmerge')
		return `<span title="DO NOT MERGE"><svg xmlns="http://www.w3.org/2000/svg" class="${classes} ${active ? 'prh-icon-donotmerge-active' : ''} ${sizeClass} prh-icon-donotmerge" viewBox="0 0 512 512"><path d="M256 8C119.034 8 8 119.033 8 256s111.034 248 248 248 248-111.034 248-248S392.967 8 256 8zm130.108 117.892c65.448 65.448 70 165.481 20.677 235.637L150.47 105.216c70.204-49.356 170.226-44.735 235.638 20.676zM125.892 386.108c-65.448-65.448-70-165.481-20.677-235.637L361.53 406.784c-70.203 49.356-170.226 44.736-235.638-20.676z"/></svg></span>`;

	if (name === 'epic')
		return `<span title="EPIC"><svg xmlns="http://www.w3.org/2000/svg" class="${classes} ${active ? 'prh-icon-epic-active' : ''} ${sizeClass} prh-icon-epic" viewBox="0 0 512 512"><path fill-rule="evenodd" d="M451.047619,243.809524 L414.47619,243.809524 L414.47619,146.285714 C414.47619,119.466667 392.533333,97.5238095 365.714286,97.5238095 L268.190476,97.5238095 L268.190476,60.952381 C268.190476,27.3066667 240.88381,0 207.238095,0 C173.592381,0 146.285714,27.3066667 146.285714,60.952381 L146.285714,97.5238095 L48.7619048,97.5238095 C21.9428571,97.5238095 0.243809524,119.466667 0.243809524,146.285714 L0.243809524,238.933333 L36.5714286,238.933333 C72.8990476,238.933333 102.4,268.434286 102.4,304.761905 C102.4,341.089524 72.8990476,370.590476 36.5714286,370.590476 L0,370.590476 L0,463.238095 C0,490.057143 21.9428571,512 48.7619048,512 L141.409524,512 L141.409524,475.428571 C141.409524,439.100952 170.910476,409.6 207.238095,409.6 C243.565714,409.6 273.066667,439.100952 273.066667,475.428571 L273.066667,512 L365.714286,512 C392.533333,512 414.47619,490.057143 414.47619,463.238095 L414.47619,365.714286 L451.047619,365.714286 C484.693333,365.714286 512,338.407619 512,304.761905 C512,271.11619 484.693333,243.809524 451.047619,243.809524 Z"/></svg></span>`;
}