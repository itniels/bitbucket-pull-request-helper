'use strict';

/**
 * Contains logic for the icon popup menu when clicking it.
 */

// Get buttons
let toggleState = document.getElementById('toggleState');
let toggleTags = document.getElementById('toggleTags');
let toggleIcons = document.getElementById('toggleIcons');
let toggleIP = document.getElementById('toggleIP');
let toggleDiffNumber = document.getElementById('toggleDiffNumber');
let toggleMerge = document.getElementById('toggleMerge');
let options = document.getElementById('options');

// Declare Variables
let isActive = true;
let showTags = true;
let useIcons = true;
let showIssueNumbers = true;
let diffNumber = true;
let disableMerge = true;

/** Get values from storage on load and change button color/state */
chrome.storage.sync.get('isActive', function (data) {
	isActive = data ? data.isActive : true;
	changeButtonState(toggleState, isActive);
});
chrome.storage.sync.get('showTags', function (data) {
	showTags = data ? data.showTags : true;
	changeButtonState(toggleTags, showTags);
	changeButtonEnabled(toggleTags, isActive);
});
chrome.storage.sync.get('useIcons', function (data) {
	useIcons = data ? data.useIcons : true;
	changeButtonState(toggleIcons, useIcons);
	changeButtonEnabled(toggleIcons, isActive);
});
chrome.storage.sync.get('showIssueNumbers', function (data) {
	showIssueNumbers = data ? data.showIssueNumbers : true;
	changeButtonState(toggleIP, showIssueNumbers);
	changeButtonEnabled(toggleIP, isActive);
});
chrome.storage.sync.get('diffNumber', function (data) {
	diffNumber = data ? data.diffNumber : true;
	changeButtonState(toggleDiffNumber, diffNumber);
	changeButtonEnabled(toggleDiffNumber, isActive);
});
chrome.storage.sync.get('disableMerge', function (data) {
	disableMerge = data ? data.disableMerge : true;
	changeButtonState(toggleMerge, disableMerge);
	changeButtonEnabled(toggleMerge, isActive);
});

// Helpers ======================================================
/** Changes the button state color (enabled/disabled visual state) */
function changeButtonState(button, state) {
	if (state == true)
		button.classList.add('green');
	else
		button.classList.remove('green');
}

/** Enables/disables a button */
function changeButtonEnabled(button, state) {
	if (state == false) {
		button.setAttribute('disabled', 'disabled');
		button.classList.add('grey');
	}
	else {
		button.removeAttribute('disabled');
		button.classList.remove('grey');
	}
}

// Button bindings ==============================================
document.addEventListener('DOMContentLoaded', function () {
	toggleState.addEventListener('click', function () {
		isActive = !isActive;
		chrome.storage.sync.set({ isActive: isActive });
		changeButtonState(toggleState, isActive);

		changeButtonEnabled(toggleTags, isActive);
		changeButtonEnabled(toggleIP, isActive);
		changeButtonEnabled(toggleMerge, isActive);
		changeButtonEnabled(toggleDiffNumber, isActive);
		changeButtonEnabled(toggleIcons, isActive);
	});

	toggleTags.addEventListener('click', function () {
		showTags = !showTags;
		chrome.storage.sync.set({ showTags: showTags });
		changeButtonState(toggleTags, showTags);
	});
	
	toggleIcons.addEventListener('click', function () {
		useIcons = !useIcons;
		chrome.storage.sync.set({ useIcons: useIcons });
		changeButtonState(toggleIcons, useIcons);
	});

	toggleIP.addEventListener('click', function () {
		showIssueNumbers = !showIssueNumbers;
		chrome.storage.sync.set({ showIssueNumbers: showIssueNumbers });
		changeButtonState(toggleIP, showIssueNumbers);
	});

	toggleDiffNumber.addEventListener('click', function () {
		diffNumber = !diffNumber;
		chrome.storage.sync.set({ diffNumber: diffNumber });
		changeButtonState(toggleDiffNumber, diffNumber);
	});

	toggleMerge.addEventListener('click', function () {
		disableMerge = !disableMerge;
		chrome.storage.sync.set({ disableMerge: disableMerge });
		changeButtonState(toggleMerge, disableMerge);
	});

	options.addEventListener('click', function () {
		chrome.runtime.openOptionsPage();
	});
});