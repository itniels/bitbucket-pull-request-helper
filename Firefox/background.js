'use strict';
/**
 * Contains logic for installation and load
 */

/** Set initial values on extension install and update if no value has been set */
chrome.runtime.onInstalled.addListener(function () {
  chrome.storage.sync.get('isActive', function (data) {
    if (!data.isActive)
      chrome.storage.sync.set({ isActive: true });
  });
  chrome.storage.sync.get('showTags', function (data) {
    if (!data.showTags)
      chrome.storage.sync.set({ showTags: true });
  });
  chrome.storage.sync.get('useIcons', function (data) {
    if (!data.useIcons)
      chrome.storage.sync.set({ useIcons: true });
  });
  chrome.storage.sync.get('showIssueNumbers', function (data) {
    if (!data.showIssueNumbers)
      chrome.storage.sync.set({ showIssueNumbers: true });
  });
  chrome.storage.sync.get('diffNumber', function (data) {
    if (!data.diffNumber)
      chrome.storage.sync.set({ diffNumber: true });
  });
  chrome.storage.sync.get('disableMerge', function (data) {
    if (!data.disableMerge)
      chrome.storage.sync.set({ disableMerge: false });
  });
  chrome.storage.sync.get('stash', function (data) {
    if (!data.stash)
      chrome.storage.sync.set({ stash: 'bitbucket.org' });
  });  
});